﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace p3
{
    public abstract class Output
    {
        public abstract void write(Object o);
    }

    public class StreamOutput : Output
    {
        private StreamWriter sink;
        public StreamOutput(StreamWriter stream)
        {
            sink = stream;
        }
        public override void write(Object o)
        {
            try
            {
                sink.Write(o.ToString());
            }
            catch (IOException ex)
            {
                throw new IOException("Error", ex);
            }
        }
    }
}
