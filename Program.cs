﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace p3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("\nPlease name of file to decorate: ");
            string readFile = Console.ReadLine();

            Console.Write("\nPlease enter name of file to test output: ");
            string writeTest = Console.ReadLine();

            using (StreamWriter writer = new StreamWriter(writeTest))
            {
                using (StreamReader reader = new StreamReader(readFile))
                {
                    Output output = new StreamOutput(writer);
                    string choice = " ", line = " ";
                    while (choice != "0")
                    {
                        printMenu();
                        choice = Console.ReadLine();
                        switch (choice)
                        {
                            case "1":
                                output = new LineOutput(output);
                                break;
                            case "2":
                                output = new NumberedOutput(output);
                                break;
                            case "3":
                                teeMenu();
                                string teefile = Console.ReadLine();
                                StreamWriter teewrite = new StreamWriter(teefile);
                                output = new TeeOutput(output, teewrite);
                                break;
                            case "4":
                                predMenu();
                                string predChoice = Console.ReadLine();
                                if (predChoice == "8")
                                {
                                    ContainsDigit cd = new ContainsDigit();
                                    output = new FilterOutput(output, cd);
                                }
                                else if (predChoice == "9")
                                {
                                    NoUpperCase noUp = new NoUpperCase();
                                    output = new FilterOutput(output, noUp);
                                }
                                break;
                            case "0":
                                while ((line = reader.ReadLine()) != null)
                                    output.write(line);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            Console.ReadKey();
        }

        static void printMenu()
        {
            Console.WriteLine(" 1 - LineOutput -> adds newline.");
            Console.WriteLine(" 2 - NumberOutput -> show line number, add newline.");
            Console.WriteLine(" 3 - TeeOutput -> writes to two stream.");
            Console.WriteLine(" 4 - FilterOutput -> conditional filter.");
            Console.WriteLine(" 0 - Done");
            Console.Write("Enter Choice: ");
        }

        static void predMenu()
        {
            Console.WriteLine("\n 8 - ContainsDigit -> only lines with digit.");
            Console.WriteLine(" 9 - NoUpperCase -> only lines that have all lowercase.");
            Console.Write("Please Enter choice: ");
        }

        static void teeMenu()
        {
            Console.Write("\nEnter the name of file to tee out: ");
        }
    }
}
