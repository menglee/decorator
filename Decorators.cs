﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace p3
{
    public class LineOutput : Output
    {
        private Output output;
        public LineOutput(Output op)
        {
            output = op;
        }
        public override void write(Object o)
        {
            output.write(o.ToString() + Environment.NewLine);
        }
    }

    public class NumberedOutput : Output
    {
        private Output output;
        private int lineNumber;
        public NumberedOutput(Output op)
        {
            lineNumber = 0;
            output = op;
        }
        public override void write(Object o)
        {
            lineNumber++;
            output.write(string.Format("{0, 5}", lineNumber) + ": " + o.ToString() + Environment.NewLine);
        }
    }

    public class TeeOutput : Output
    {
        private Output output;
        private StreamWriter secondStream;
        public TeeOutput(Output op, StreamWriter stream)
        {
            output = op;
            secondStream = stream;
        }
        public override void write(Object o)
        {
            Output newOut = new StreamOutput(secondStream);
            newOut.write(o.ToString() + Environment.NewLine);
            output.write(o.ToString() + Environment.NewLine);
        }
    }

    public interface Predicate
    {
        bool execute(Object o);
    }

    public class FilterOutput : Output
    {
        private Output output;
        private Predicate pred;
        public FilterOutput(Output op, Predicate p)
        {
            output = op;
            pred = p;
        }
        public override void write(Object o)
        {
            if (pred.execute(o))
                output.write(o.ToString() + "\n");
        }
    }

    public class ContainsDigit : Predicate
    {
        public bool execute(Object o)
        {
            string line = o.ToString();
            foreach (char c in line)
            {
                if (char.IsDigit(c))
                    return true;
            }
            return false;
        }
    }

    public class NoUpperCase : Predicate
    {
        public bool execute(Object o)
        {
            string line = o.ToString();
            foreach (char c in line)
            {
                if (char.IsUpper(c))
                    return false;
            }
            return true;
        }
    }
}
